$(document).ready(function(){
	$(document).foundation();

	$('.title-bar-left #menuButton').click(function() {
	  if ($(this).hasClass('is-active')) {
	  	$(this).removeClass('is-active');
	  	$('#offCanvasLeft').foundation('toggle');
	  } else {
	  	$(this).addClass('is-active');
	  	$('#offCanvasLeft').foundation('toggle');
	  }
	});
	
	var search_placeholder = function() {
		if ($(window).width() > 640) {
			$('.home-search input').attr('placeholder', 'Search website eg. council tax');
		} else {
			$('.home-search input').attr('placeholder', 'Search website');
		}
	}

	if ($('.home').length > 0) {
		search_placeholder();
		
		$(".top-tasks-carousel").owlCarousel({
			items: 4,
			dots: false,
			loop: true,
			slideBy: 4,
			responsive : {
			    // breakpoint from 0 up
			    0 : {
			      	items: 1,
			        slideBy: 2
			    },
			    // breakpoint from 640 up
			    640 : {
			        items: 2,
			        slideBy: 2
			    },
			    // breakpoint from 768 up
			    768 : {
			        items: 3,
			        slideBy: 3
			    },
			    1025 : {
			        items: 4,
			        slideBy: 4
			    }
			}
		});

		$('.latest-news-carousel').owlCarousel({
			items: 3,
			dots: false,
			loop: true,
			responsive : {
			    // breakpoint from 0 up
			    0 : {
			      items: 1,
			      slideBy: 1
			    },
			    // breakpoint from 640 up
			    768 : {
			        items: 3,
			        slideBy: 3
			    },
			    1440 : {
			        items: 4,
			        slideBy: 4
			    }
			}
		});

		$( window ).resize(function() {
			search_placeholder();
			init_top_tasks_carousel();
		});

		// Council services click dropdown menu
		$('.council-services ul > li > a').click(function(e) {
			if ($(window).width() > 640) {
				e.preventDefault();
			  	if ($(this).closest('li').hasClass('menu-open')) {
			  		$(this).closest('li').removeClass('menu-open');
			  		$(this).closest('ul').removeClass('dropped');
			  	} else {
			  		$(this).closest('li').siblings().removeClass('menu-open');
			  		$(this).closest('li').addClass('menu-open');
			  		$(this).closest('ul').addClass('dropped');
				}
			}
		});

	} // home

});